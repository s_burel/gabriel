# The Gabriel project

## Autorship, Version and Licence
The Gabriel project is a free book; you may reproduce and/or modify it under the terms of the Open Software License, version 3.0.
This book is distributed in the hope it will be useful, but without any warranty, without even the implied warranty of merchantability or fitness for a particular purpose.
The author of this project is Stéphane Burel.
This project may contains some errors and do not have the pretension to be perfect. If you spot a problem, if you have a question, or if you want to contribute fell free to contact me at stephane.burel.toulet@gmail.com. Your comments are really welcomed !

Please have a look to the pdf that explain a lot more in the pdf/ directory.