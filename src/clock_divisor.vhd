------------------------------------------------------------------------------
-- Clock Divisor Source
------------------------------------------------------------------------------
-- That Clock Divisor get in input a clock,
-- And release the same clock cadenced 100 time slower
-- It works according to a simple Finite State Machine
------------------------------------------------------------------------------
-- Ports :
--   in    : clk     : Input clock
--           reset   : Used to reset the component
--   out   : clk_out : The slowed clock
------------------------------------------------------------------------------
-- Used signals and variables
--   current_s : Define the state your component is actually in
--   next_s    : Define the state your component will enter in on the next
--               rising edge of the clock
--   cpt       : Counter used to count the numer of rising edge
--               of the in clock
--   out_reg   : One bit register used to store the output value
------------------------------------------------------------------------------
-- Different state of the finite state machine :
--   reset_s  : (initial state) Happen if the reset bit is low
--                out_reg is set to 0
--                Next state is always start_s
--   start_s    : Next state is always inc_s
--                Counter is set to 0
--   inc_s      : If counter is equal to 47, then next state is overflow_s
--                Else, The next state still inc_s
--                The state change when the counter equals 47 and not 50 due
--                  to the cycle used by the state transition. So the Clock
--                  frequency is exactly divided by 100
--   overflow_s : Change the out_reg value to its negation.
--                Next state is always start_s
------------------------------------------------------------------------------
-- List of VHDL process :
-- cpt_handler : That process handle the counter, increasing the counter
--               on each rising edge of the input clock, except when the
--               current state of the FSM is the start state
-- next_state  : Process used to handle the next_state and the out register
--               according to the current state
-- clocked     : Process synchronized to the input clock that change the
--               current state with its next value.
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity clock_divisor is
   port( clk     : in  std_logic;
         reset   : in  std_logic;
         clk_out : out std_logic);

end clock_divisor;
--
architecture behaviour of clock_divisor is

  type state_type is (reset_s, start_s, inc_s, overflow_s);

  signal current_s : state_type := reset_s;
  signal next_s    : state_type;

  signal out_reg : std_logic := '0';
  signal cpt : integer range 0 to 50; 

begin  

-- Process that handle the counter
cpt_handler : process( clk )
begin
if rising_edge(clk) then
  if current_s = start_s then
    cpt <= 0;
  elsif current_s = inc_s then
    cpt <= cpt + 1;
  end if ;
end if ;
end process ; -- cpt_handler

clk_out_handler : process (clk)
begin
    if rising_edge(clk) then
    clk_out <= out_reg;
    end if;
end process ;

out_reg_handler : process( clk )
begin
if rising_edge(clk) then
    if current_s = reset_s then
        out_reg <= '0';
    elsif current_s = overflow_s and next_s /= overflow_s then
        out_reg <= not out_reg;
    else
        out_reg <= out_reg;
    end if;
end if ;
end process ;

-- Process that handle the next state and the out_reg register
next_state : process( clk )
begin
if (rising_edge(clk)) then
  case( current_s ) is
    when reset_s =>
     next_s <= start_s;
    when start_s =>
      next_s <= inc_s;
    when inc_s =>
      if cpt = 44 then
        next_s <= overflow_s;
      else
       next_s <= next_s;
      end if ;
   when overflow_s =>
      next_s <= start_s;
    when others =>
      next_s <= next_s;
  end case ;
end if ;
end process ; -- next_state

-- Process that affect the current state to new state
clocked : process( clk, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

--clk_out <= out_reg;

end behaviour;