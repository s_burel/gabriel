------------------------------------------------------------------------------
-- Frame Register Source
------------------------------------------------------------------------------
-- One DCC packet can activate one or many functions.
-- That component contains one register that store, for each functions,
-- ont bit that inform if the function is actived, or not.
-- That component receive a 20 bits value from the user interface, coded as :
-- First 8 bits (bits 0 to 7)  : Address of the models
-- Next 5 bits (bits 8 to 12)  : Order given to the models (the project
--                               can code 20 differents functions from the 
--                               NMRA standard plus the movement order)
-- Next 6 bits (bits 13 to 18) : Speed ordered to the model (only used if the
--                               order given to the model is a movement order)
-- Next bit (bit 19)           : Validity bits of the request
-- The compoment then check if it has to activate, or desactivate the function
-- called, and insert other function from the same groups in the out order.
-- The compoment finally send to the frame constructor a 32 bits signals coded
-- as : 
-- Bits  0 to  7 : address
-- Bits  8 to 23 : Bits used to build the frame
--                 Contains all the functions sent and the way to code them
--                 Can change according to the group of functions (4 groups :
--                 function 0 to 4, 5 to 8, 9 to 12 and speed function)
-- Bit 24        : Validy bit
-- Bits 25 to 31 : Yet unused, but can be used if we then use more functions
------------------------------------------------------------------------------
-- There are plenty of different functions described by the NMRA standard,
-- but our project deals with only 21 differents functions able
-- to be run by your DCC models
-- Function 00 -> Light on/off
-- Function 01 -> All sounds on/off
-- Function 02 -> Sound : Horn #1
-- Function 03 -> Sound : Horn #2
-- Function 04 -> Sound : Turbo Engine
-- Function 05 -> Sound : Compressor
-- Function 06 -> Sound : Acceleration
-- Function 07 -> Sound : Squeak of curve
-- Function 08 -> Sound : Railway clank
-- Function 09 -> Sound : Fan
-- Function 10 -> Sound : Signal
-- Function 11 -> Sound : Short Horn #1
-- Function 12 -> Sound : Short Horn #2
-- Function 13 -> Sound : Railway Station Announcement #1
-- Function 14 -> Sound : Railway Station Announcement #2
-- Function 15 -> Sound : Alert signal #1
-- Function 16 -> Sound : Alert signal #2
-- Function 17 -> Sound : Doors open/closed
-- Function 18 -> Sound : Valve
-- Function 19 -> Sound : Hitch
-- Function 20 -> Sound : Sand
-- Function 21 -> Set speed of your model
------------------------------------------------------------------------------
-- These differents functions are grouped in 5 differents groups, according
-- to the coding of these function in the DCC packet.
-- Group 1 : Function 0 to 4 :
--     Coded on one byte in DCC packet as :
--         100XXXXX -> (for each X (1 = function on)(0 = function off))
--             Bit 4      : Function 0
--             Bit 3 to 0 : Function 4 to 1
-- Group 2 : Function 5 to 8 :
--     Coded on one byte in DCC packet as :
--         1011XXXX -> (for each X (1 = function on)(0 = function off))
--             Bit 3 to 0 : Function 8 to 5
-- Group 3 : Function 9 to 12 :
--     Coded on one byte in DCC packet as :
--         1010XXXX -> (for each X (1 = function on)(0 = function off))
--             Bit 3 to 0 : Function 12 to 9
-- Group 4 : Function 13 to 20 :
--     Coded on two bytes in DCC packet as :
--         11011110 XXXXXXXX
--             Bits 7 to 0 off second byte : Function 20 to 13
-- Group 5 : Speed function :
--     Coded on one byte on DCC packet as :
--         01XXXXXX -> (each X describe the commanded speed)
------------------------------------------------------------------------------
-- Ports :
-- in    clk           : std_logic : Clock used to control the fsm.
--       reset         : std_logic : Used to reset the component
--       command       : std_logic_vector (20) : The value of the order the
--                                   user is entering. That port respect the
--                                   convention previously defined :
--                                     First 8 bits (bits 0 to 7)  : Address
--                                       of the models
--                                     Next 5 bits (bits 8 to 12)  : Order
--                                       given to the models (the project can
--                                       code 20 differents functions from the
--                                       NMRA standard plus the movement
--                                       order)
--                                     Next 6 bits (bits 13 to 18) : Speed 
--                                       ordered to the model (only used if
--                                       the order given to the model is a
--                                       movement order)
--                                     Next bit (bit 19) : Validity bits of 
--                                       the request
-- out   tr_to_tc_order : std_logic_vector (32) : The -- The command send to
--                        the frame constructor a 32 bits signals coded as :
--                        Bits  0 to  7 : address
--                        Bits  8 to 23 : Bits used to build the frame
--                                        Contains all the functions sent and
--                                        the way to code them. Can change
--                                        according to the group of functions
--                                        (4 groups : functions 0 to 4,
--                                        5 to 8, 9 to 12 and speed function)
--                        Bit 24        : Validy bit
--                        Bits 25 to 31 : Yet unused, but can be used if we
--                                        then use more functions
------------------------------------------------------------------------------
--Used signals and variables
-- current_s      : Define the state your component is actually in
-- next_s         : Define the state your component will enter in on the next
--                  rising edge of the clock
  -- order_register : A 32 bit register that store the status (on/off) for
  --                  each functions, their number define the index on the
  --                  register
-- validity_out   : Image of the bit 24 of the out port tr_to_tc_order.
--                  Used to store the validity of the order.
-- address        : Image of the bits (7 to 0) of the in port command.
--                  Used to store the value of the adresse the model the
--                  user want to interact with
-- order          : Image of the bits (8 to 12) of the in port command.
--                  Used to store the value the order the user want to
--                  send to the model
-- speed          : Image of the bits (13 to 18) of the in port command.
--                  Used to store the speed ordered to the model (only used
--                  if the order given to the model is a movement order)
-- validity_in    : Image of the bits (19) of the port out command. 
--                  Used to store the validity of the order. 0 mean the
--                  order is not valid. 1 mean the order is valid
------------------------------------------------------------------------------
--Different state of the finite state machine :
-- reset_s       : (initial state) The register of the functions is reset to 0, which mean all the functions are off. Next state is always address_s
-- start_s       : Wait for the input command to be valid, in which case the next state is new_command_s. Else, the state still start_s
-- new_command_s : Update the register containing all the function according to the new received order. Next state is always validate_s
-- validate_s    : Spread the new command according to the updated register.
--                 The only state where the out value is valid.
--                 If the input order is valid, stay in same validate_s state.
--                 When the input order is unvalided, next state is start_s
------------------------------------------------------------------------------
--List of VHDL process :
-- validity_out_handler : Triggering signal : current_s
--                        That process handle the value of the validity bit
--                        of the output order.
-- set_order            : Triggering signal : current_s
--                        That process handle the value of the output order.
-- update_register      : Triggering signal : current_s
--                        That process handle the register containing the
--                        status for each function.
-- next_state           : Triggering signal : current_s, validity_in
--                        Process used to handle the next_state value
-- clocked              : Triggering signal : clk, reset
--                        Process synchronized to the input clock that change
--                        the current state with its next value.
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity frame_register is
  port( clk            : in std_logic;
        reset          : in std_logic;
        command        : in std_logic_vector(19 downto 0);
        tr_to_tc_order : out  std_logic_vector(31 downto 0));
end frame_register;
--
architecture behaviour of frame_register is

  type state_type is 
    ( reset_s, start_s, new_command_s, validate_s );

  signal current_s : state_type := reset_s;
  signal next_s    : state_type;
  signal order_register : std_logic_vector(31 downto 0);
  --signal validity_out : std_logic;
  signal address        : std_logic_vector(7 downto 0);
  signal order          : std_logic_vector(4 downto 0);
  signal speed          : std_logic_vector(5 downto 0);
  signal validity_in    : std_logic;


attribute keep : string;

attribute keep of order_register : signal is "true";

begin

address     <= command(7 downto 0);
order       <= command(12 downto 8);
speed       <= command(18 downto 13);
validity_in <= command(19);

--tr_to_tc_order(24) <= validity_out;

-- validity_out_handler : Triggering signal : current_s
--                        That process handle the value of the validity bit
--                        of the output order.
--validity_out_handler : process( current_s )
--begin
--  case( current_s ) is
--    when validate_s =>
--      validity_out <= '1';
--    tr_to_tc_order(24) <= '1';
--    when others =>
--    tr_to_tc_order(24) <= '0';
--      validity_out <= '0';
--  end case ;
--end process ; -- validity_out_handler

-- set_order            : Triggering signal : current_s
--                        That process handle the value of the output order.
set_order : process( current_s, address, order, order_register, speed )
begin
  if current_s = validate_s then
  -- Address
      tr_to_tc_order(7 downto 0) <= address;
      tr_to_tc_order(24) <= '1';
  -- Fonctions 0 à 4
    if unsigned(order) < 5 then
      tr_to_tc_order(12 downto 8) <= "00000";
      tr_to_tc_order(15 downto 13) <= "100";
      tr_to_tc_order(20 downto 16) 
        <= order_register(0) & order_register(4 downto 1);
      tr_to_tc_order(23 downto 21) <= "000";
      tr_to_tc_order(31 downto 25) <= "0000000";
  -- Fonctions 5 à 8
    elsif unsigned(order) < 9 then
      tr_to_tc_order(11 downto 8) <= "0000";
      tr_to_tc_order(15 downto 12) <= "1011";
      tr_to_tc_order(19 downto 16) <= order_register(8 downto 5);
      tr_to_tc_order(23 downto 20) <= "0000";
      tr_to_tc_order(31 downto 25) <= "0000000";
  -- Fonctions 9 à 12
    elsif unsigned(order) < 13 then
      tr_to_tc_order(11 downto 8) <= "0000";
      tr_to_tc_order(15 downto 12) <= "1010";
      tr_to_tc_order(19 downto 16) <= order_register(12 downto 9);
      tr_to_tc_order(23 downto 20) <= "0000";
      tr_to_tc_order(31 downto 25) <= "0000000";
  -- Fonctions 13 à 20
    elsif unsigned(order) < 21 then
      tr_to_tc_order(15 downto 8) <= "11011110";
      tr_to_tc_order(23 downto 16) <= order_register(20 downto 13);
      tr_to_tc_order(31 downto 25) <= "0000000";
  -- Speed function
    else
      tr_to_tc_order(13 downto 8) <= "000000";
      tr_to_tc_order(15 downto 14) <= "01";
      tr_to_tc_order(21 downto 16) <= speed;
      tr_to_tc_order(23 downto 22) <= "00";
      tr_to_tc_order(31 downto 25) <= "0000000";
    end if ;
  else
   tr_to_tc_order <= "00000000000000000000000000000000";
  end if ;
end process ; -- set_order

-- update_register      : Triggering signal : current_s
--                        That process handle the register containing the
--                        status for each function.
update_register : process( clk )
begin
if rising_edge(clk) then
    identifier : for i in 0 to 31 loop
      if current_s = reset_s then
        order_register(i) <= '0';
      elsif i = unsigned(order) and current_s = new_command_s then
        order_register(i) <= not order_register(i);
      else
        order_register(i) <= order_register(i);
      end if ;
    end loop ;
end if ;
end process ; -- update_register

-- next_state           : Triggering signal : current_s, validity_in
--                        Process used to handle the next_state value
next_state : process  ( current_s, validity_in )
begin
  case( current_s ) is
    when reset_s =>
        next_s <= start_s;
    when start_s =>
      if validity_in = '1' then
        next_s <= new_command_s;
      else
        next_s <= start_s;
      end if ;
    when new_command_s =>
      next_s <= validate_s;
    when validate_s =>
      if validity_in = '1' then
        next_s <= validate_s;
      else
        next_s <= start_s;
      end if ;
    when others =>
      NULL;
  end case ;
end process ; -- next_state

-- clocked              : Triggering signal : clk, reset
--                        Process synchronized to the input clock that change
--                        the current state with its next value.
clocked : process( clk, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;