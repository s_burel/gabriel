------------------------------------------------------------------------------
-- User Interface Source
------------------------------------------------------------------------------
-- That component receive some value controled by three buttons.
-- This componet is the first layer between the user (via the buttons) and
-- all the system. This component is coded so a signal linked to a button
-- equal 1 when the button is pushed, else its value is 0.
-- The user has to enter some value successively and then the value are
-- transmetted to order given to other components.
-- This component is linked to 8 leds so the user can see what value he
-- is inserting.
-- The behaviour of this component is described by a simple FSM.
-- The component is candenced by a 10 Khz clock.
-- The order the user in entering is coded to 20 bits, linked to an out port
-- named command and who respect the following convention :
-- First 8 bits (bits 0 to 7)  : Address of the models
-- Next 5 bits (bits 8 to 12)  : Order given to the models (the project
--                               can code 20 differents functions from the 
--                               NMRA standard plus the movement order)
-- Next 6 bits (bits 13 to 18) : Speed ordered to the model (only used if the
--                               order given to the model is a movement order)
-- Next bit (bit 19)           : Validity bits of the request
-- When an order is validated by the user, the user-interface component enter
-- in a state where the ordered is validated so long that we are sure a full
-- DCC packet has the time to be sent and the next order (the validated one)
-- is validated. As the maximum time of a DCC packet plus the followed delay
-- is equals to +- 7 ms, we wait for 100 ms so we are sure the order is
-- passed to the DCC models.
------------------------------------------------------------------------------
--Ports :
-- in    clk           : std_logic : Clock used to control the fsm and to
--                                   handle the counter. One cadency of 10Khz
--                                   and a counter will help to prevent the
--                                   boucing of buttons. (As low quality
--                                   buttons can bounce as long as 80ms after
--                                   the pushing, count 100 rising edge of a 
--                                   clock cadenced at 10 Khz equal to a delay
--                                   of 100 ms, which should cover all
--                                   bouncing)
--       reset         : std_logic : Used to reset the component
--       button_left   : std_logic : Buttons used as input interface to the
--       button_right  : std_logic   user. The left button decrease the value
--       button_center : std_logic   the user is inserting. The right button
--                                   increase the latter. And the third button
--                                   valid the current input to insert a new
--                                   value
-- out   command       : std_logic_vector (20) : The value of the order the
--                                   user is entering. That port respect the
--                                   convention previously defined :
--                                     First 8 bits (bits 0 to 7)  : Address
--                                       of the models
--                                     Next 5 bits (bits 8 to 12)  : Order
--                                       given to the models (the project can
--                                       code 20 differents functions from the
--                                       NMRA standard plus the movement
--                                       order)
--                                     Next 6 bits (bits 13 to 18) : Speed 
--                                       ordered to the model (only used if
--                                       the order given to the model is a
--                                       movement order)
--                                     Next bit (bit 19) : Validity bits of 
--                                       the request
-- out   leds          : std_logic_vector (8) : 8 leds used to describe the
--                                     value the user is inserting
------------------------------------------------------------------------------
--Used signals and variables
-- current_s : Define the state your component is actually in
-- next_s    : Define the state your component will enter in on the next
--             rising edge of the clock
-- buffer_s  : Define the state your component will enter in when it will
--             exit the tempo_s state
-- cpt       : Counter used to count the numer of rising edge of the clock
-- address   : Image of the bits (7 to 0) of the port out command. Used to
--             store the value of the adresse the model the user want to
--             interact with
-- order     : Image of the bits (8 to 12) of the port out command. Used to
--             store the value the order the user want to send to the model
-- speed     : Image of the bits (13 to 18) of the port out command. Used to
--             store the speed ordered to the model (only used if the order
--             given to the model is a movement order)
-- validity  : Image of the bits (19) of the port out command. Used to
--             store the validity of the order. 0 mean the oder is not valid.
--             1 mean the order is valid
------------------------------------------------------------------------------
--Different state of the finite state machine :
-- reset_s    : (initial state) All the buffer are reset to 0.
--              Next state is always address_s
-- tempo_s    : Increment the counter up to 100 (linked with a 10 Khz clock,
--              it mean 100 ms) and jump to the state set in buffer_s as
--              soon as all the buttons are released
-- address_s  : State in which the user has to enter the address of the model
--              the user want to interract with
--              If right or left buttons are pushed, then following state
--                will be tempo_s, and then address_s
--              If center button is pushed, then the folliwing state
--                will be fn_s
-- fn_s       : State in which the user has to enter the order number of the
--              function he want to give
--              If right or left buttons are pushed, then following state
--                will be tempo_s, and then fn_s
--              If center button is pushed, then the folliwing state
--                will be speed_s if the ordered function is the mouvement
--                function, else it will be validate_s
--              Else, the nexte state will be speed_s
-- speed_s    : State in which the user has to enter the speed of the model
--              he want to order
--              If right or left buttons are pushed, then following state
--                will be tempo_s, and then speed_s
--              If center button is pushed, then the folliwing state
--                will be validate_s
--              Else, the nexte state will be speed_s
-- validate_s : State where the order is validated.
--              Following state will be tempo_s, and then address_s
------------------------------------------------------------------------------
--List of VHDL process :
-- address_handler : Triggering signal : current_s, button_left, button_right
--   That process handle the increment or the decrement of the
--   address of the model the user want to use
-- order_handler   : Triggering signal : current_s, button_left, button_right
--   That process handle the increment or the decrement of the
--   the number of the function the user want to send
-- leds_handler    : Triggering signal : current_s
--   That process handle the leds that display the value the user is
--   inserting
-- speed_handler   : Triggering signal : current_s, button_left, button_right
--   That process handle the increment or the decrement of the
--   the speed the user want to order
-- cpt_hander      : Triggering signal : clk
--   That process handle the counter used by the tempo_s state
-- validation_handler : Triggering signal : current_s
--   That process handle the validity bit of the command
--next_state : Triggering signal : current_s, cpt, buttons
--                       Process used to handle the next_state and
--                       the buffer_s according to the current state,
--                       the counter and the buttons.
--clocked             : Triggering signal : clk, reset
--                       Process synchronized to the input clock that change
--                       the current state with its next value.
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity user_interface is
   port( clk           : in std_logic;
         reset         : in  std_logic;
         button_left   : in  std_logic;
         button_right  : in  std_logic;
         button_center : in  std_logic;
         command       : out std_logic_vector(19 downto 0);
         leds          : out std_logic_vector(7 downto 0));
end user_interface;
--
architecture behaviour of user_interface is

  type state_type is ( reset_s, tempo_s, address_s, fn_s, speed_s, validate_s );

  signal current_s : state_type := reset_s;
  signal next_s    : state_type;
  signal buffer_s  : state_type;

  signal cpt     : integer range 0 to 60001;

  signal address  : std_logic_vector(7 downto 0);
  signal order    : std_logic_vector(4 downto 0);
  signal speed    : std_logic_vector(5 downto 0);
  signal validity : std_logic;

begin

process( address, validity, speed, order )
begin
    command(7 downto 0)   <= address;
    command(12 downto 8)  <= order;
    command(18 downto 13) <= speed;
    command(19)           <= validity;
end process;


-- address_handler : Triggering signal : current_s, button_left, button_right
--   That process handle the increment or the decrement of the
--   address of the model the user want to use
address_handler : process( clk )
begin
if rising_edge(clk) then
  if current_s = reset_s then
    address <= "00000000";
  elsif current_s = address_s then
    if next_s = tempo_s and button_right = '1' then
      if address = "11111111" then
        address <= "00000000";
      else
        address <= std_logic_vector(unsigned(address) + 1);
      end if ;
    elsif next_s = tempo_s and button_left = '1' then
      if address = "00000000" then
        address <= "11111111";
      else
        address <= std_logic_vector(unsigned(address) - 1);
      end if ;
    else
      address <= address;
    end if ;
  else
    address <= address;
  end if ;
end if;
end process ; -- address_handler

-- order_handler   : Triggering signal : current_s, button_left, button_right
--   That process handle the increment or the decrement of the
--   the number of the function the user want to send
order_handler : process( clk )
begin
if rising_edge(clk) then
  if current_s = reset_s then
    order <= "00000";
  elsif current_s = fn_s then
    if next_s = tempo_s and button_right = '1' then
      if order = "11111" then
        order <= "00000";
      else
        order <= std_logic_vector(unsigned(order) + 1);
      end if ;
    elsif next_s = tempo_s and button_left = '1' then
      if order = "00000" then
        order <= "11111";
      else
        order <= std_logic_vector(unsigned(order) - 1);
      end if ;
    else
      order <= order;
    end if ;
  else
    order <= order;
  end if ;
end if ;
end process ; -- order_handler

-- leds_handler    : Triggering signal : current_s
--   That process handle the leds that display the value the user is
--   inserting
leds_handler : process( clk )
begin
if rising_edge(clk) then
  case( current_s ) is
    when address_s =>
      leds <= address;
    when fn_s =>
      leds <= "000" & order;
    when speed_s =>
      leds <= "00" & speed;
    when others =>
      leds <= "11111111";
  end case ;
end if ;
end process ; -- leds_handler

-- speed_handler   : Triggering signal : current_s, button_left, button_right
--   That process handle the increment or the decrement of the
--   the speed the user want to order
speed_handler : process( clk )
begin
if rising_edge(clk) then
  if current_s = reset_s then
    speed <= "000000";
  elsif current_s = speed_s then
    if next_s = tempo_s and button_right = '1' then
      if speed = "111111" then
        speed <= "000000";
      else
        speed <= std_logic_vector(unsigned(speed) + 1);
      end if ;
    elsif next_s = tempo_s and button_left = '1' then
      if speed = "000000" then
        speed <= "111111";
      else
        speed <= std_logic_vector(unsigned(speed) - 1);
      end if ;
    else
      speed <= speed;
    end if ;
  else
    speed <= speed;
  end if ;
end if ;
end process ; -- speed_handler

-- cpt_hander      : Triggering signal : clk
--   That process handle the counter used by the tempo_s state
cpt_handler : process( clk )
begin
if rising_edge(clk) then
  if current_s = tempo_s and cpt < 100 then
    cpt <= cpt + 1;
  elsif current_s = tempo_s and cpt = 100 then
      cpt <= cpt;
  else
        cpt <= 0;
  end if ;
end if ;
end process ; -- cpt_handler

-- validation_handler : Triggering signal : current_s
--   That process handle the validity bit of the command
validation_handler : process( clk )
begin
if rising_edge(clk) then
  case( current_s ) is
    when validate_s =>
      validity <= '1';
    when tempo_s =>
      validity <= validity;
    when others =>
      validity <= '0';
  end case ;
end if;
end process ; -- validation_handler

--next_state : Triggering signal : current_s, cpt, buttons
--                       Process used to handle the next_state and
--                       the buffer_s according to the current state,
--                       the counter and the buttons.
next_state : process( clk )
begin
if rising_edge(clk) then
  case( current_s ) is
    when reset_s =>
      next_s <= address_s;
    when address_s =>
      if button_left = '1' or button_right = '1' then
        next_s <= tempo_s;
        buffer_s <= address_s;
      elsif button_center = '1' then
        next_s <= tempo_s;
        buffer_s <= fn_s;
      else
        next_s <= address_s;
      end if ;
    when fn_s =>
      if button_left = '1' or button_right = '1' then
        next_s <= tempo_s;
        buffer_s <= fn_s;
      elsif button_center = '1' then
        if unsigned(order) < 21 then
          next_s <= validate_s;
        else
          next_s <= tempo_s;
          buffer_s <= speed_s;
        end if ;
      else
        next_s <= fn_s;
      end if ;
    when speed_s =>
      if button_left = '1' or button_right = '1' then
        next_s <= tempo_s;
        buffer_s <= speed_s;
      elsif button_center = '1' then
        next_s <= validate_s;
      else
        next_s <= speed_s;
      end if ;
    when validate_s =>
      next_s <= tempo_s;  
      buffer_s <= address_s;      
    when tempo_s =>
      if cpt = 100 and 
         button_left = '0' and 
         button_right = '0' and
         button_center = '0' then
        next_s <= buffer_s;
      else
        next_s <= tempo_s;
      end if ;
    when others =>
      next_s <= next_s;
  end case ;
end if;
end process ; -- next_state

--clocked             : Triggering signal : clk, reset
--                       Process synchronized to the input clock that change
--                       the current state with its next value.
clocked : process( clk, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;