------------------------------------------------------------------------------
-- FSM Component
------------------------------------------------------------------------------
-- The FSM Component handle via some flip-flop many component.
-- It receive a value from the DCC register and sent it to DCC model via
-- the command of DCC Bit generations
------------------------------------------------------------------------------
-- Ports :
-- in    clk            : std_logic  : Clock used to control the fsm.
--       reset          : std_logic  : Used to reset the component
--       register_end   : std_logic  : Bit used to inform the FSM
--                                     component that the passed bit is
--                                     the last one of the DCC packet
--                                     Used to know if the FSM component
--                                     should then start the temporization
--                                     between two DCC packets
--       register_value : std_logic  : Bit of the DCC frame sent to the
--                                     FSM component.
-- inout command_bit_0  : std_logic  : Flip-flop to synchronize the DCC Bit 0
--                                     component and the FSM component
--       command_bit_1  : std_logic  : Flip-flop to synchronize the DCC Bit 1
--                                     component and the FSM component
--       command_tempo  : std_logic  : Flip-flop to synchronize the temporizer
--                                     and the FSM component
--       command_fsm    : std_logic  : Flip-flop to synchronize the FSM
--                                     component and the DCC register
------------------------------------------------------------------------------
-- Used signals and variables
-- current_s      : Define the state your component is actually in
-- next_s         : Define the state your component will enter in on the next
--                  rising edge of the clock
------------------------------------------------------------------------------
-- Different state of the finite state machine :
-- reset_s    : (initial state) Invalidate the mailbox (value 0)in common
--              between the FSM component and the DCC register.
--              Next state is always idle_s
-- idle_s     : Set the mailbox to High Impedance, waiting for the DCC
--              register to validate the message.
--              Stay on idle state while mailbox not valid.
--              Next state is fetch_s if mailbox is valid.
-- fetch_s    : Launch one of the DCC bit Generation via the command_bit_X
--              flip-flop.
--              Next state is according the input value bit_0_s, bit_1_s or
--              last_bit_s
-- bit_X_s    : Once the FSM is in bit_X_s state, the command_bit_X is set
--              to High Impedance, and the FSM stay on this state until the
--              command_bit_X value is a 0, acknowledging the end of the bit
--              generation. Then, the next state is reset_s
-- last_bit_s : Once the FSM is in last_bit_s state, the command_bit_1 is set
--              to High Impedance, and the FSM stay on this state until the
--              command_bit_1 value is a 0, acknowledging the end of the bit
--              generation. Then, the next state is tempo_s and the 
--              temporizer is started via the command_tempo flip-flop.
-- tempo_s    : Once the FSM is in tempo_s state, the command_tempo is set
--              to High Impedance, and the FSM stay on this state until the
--              command_tempo value is a 0, acknowledging the end of 
--              temporization. Then, the next state is reset_s.
------------------------------------------------------------------------------
-- List of VHDL process :
-- command_bit_handler   : Triggering signal : reset, current_s,
--                                             register_value, register_end
--                         That process handle the Bit Generation flip-flop
-- command_fsm_handler   : Triggering signal : reset, current_s,
--                                             register_value, register_end
--                         That process handle the DCC register flip-flop
-- command_tempo_handler : Triggering signal : reset, current_s, command_bit_1
--                         That process handle the temporizer flip-flop
-- next_state            : Triggering signal : current_s, command_fsm
--                         Process used to handle the next_state value
-- clocked               : Triggering signal : clk, reset
--                         Process synchronized to the input clock that change
--                         the current state with its next value.
---------------- --------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fsm is
   port( clk            : in  std_logic;
         reset          : in  std_logic;
         register_end   : in  std_logic;
         register_value : in  std_logic;
         command_bit_0  : inout std_logic;
         command_bit_1  : inout std_logic;
         command_tempo  : inout std_logic;
         command_fsm    : inout std_logic);
end fsm;
--

architecture behaviour of fsm is

  type state_type is 
    ( reset_s, idle_s, fetch_s, tempo_s , bit_0_s, bit_1_s, last_bit_s);

  signal current_s : state_type := reset_s;

    signal next_s    : state_type;
  
  attribute fsm_encoding : string;
    attribute fsm_encoding of current_s : signal is "one_hot";
    attribute fsm_encoding of next_s : signal is "one_hot";
  

begin

-- command_bit_handler : Triggering signal : reset, current_s, register_value
--                                           register_end
--                       That process handle the Bit Generation flip-flop
command_bit_handler : process( reset, current_s, register_value, register_end )
begin
  if reset = '0' then
    command_bit_0 <= 'Z';
  elsif current_s = bit_0_s then
    command_bit_0 <= 'Z';
  elsif current_s = fetch_s and register_end = '0' and register_value = '0' then
    command_bit_0 <= '1';
  else
    command_bit_0 <= '0';
  end if ;
  if reset = '0' then
    command_bit_1 <= 'Z';
  elsif current_s = bit_1_s then
    command_bit_1 <= 'Z';
  elsif current_s = last_bit_s then
    command_bit_1 <= 'Z';
  elsif current_s = fetch_s and register_end = '1' then
    command_bit_1 <= '1';
  elsif current_s = fetch_s and register_end = '0' and register_value = '1' then
    command_bit_1 <= '1';
  else
    command_bit_1 <= '0';
  end if ;
end process ; -- command_bit_handler

-- command_fsm_handler : Triggering signal : reset, current_s, register_value
--                                           register_end
--                       That process handle the DCC register flip-flop
command_fsm_handler : process( reset, current_s, register_value, register_end )
begin
  if current_s = idle_s then
    command_fsm <= 'Z';
  elsif current_s = reset_s then
    command_fsm <= '0';
  else
    command_fsm <= '1';
  end if ;
end process ; -- command_bit_handler

-- command_tempo_handler : Triggering signal : reset, current_s, command_bit_1
--                         That process handle the temporizer flip-flop
command_tempo_handler : process( reset, current_s, command_bit_1 )
begin
  if current_s = tempo_s then
    command_tempo <= 'Z';
  elsif current_s = last_bit_s and command_bit_1 = '0' then
    command_tempo <= '1';
  else
    command_tempo <= '0';
  end if ;
end process ; -- command_bit_handler

-- next_state            : Triggering signal : current_s, command_fsm
--                         Process used to handle the next_state value
next_state : process(clk)
--  ( current_s, command_fsm, command_bit_0, command_bit_1, command_tempo )
begin
if rising_edge(clk) then
  case( current_s ) is
    when reset_s =>
      next_s <= idle_s;
    when idle_s =>
      if command_fsm = '1' then
        next_s <= fetch_s;
      else
        next_s <= next_s;
      end if ;
    when fetch_s =>
      if register_end = '1' then
        next_s <= last_bit_s;
      elsif register_value = '1' then
        next_s <= bit_1_s;
      else
        next_s <= bit_0_s;
      end if ;
    when bit_0_s =>
      if command_bit_0 = '0' then
        next_s <= reset_s;
      else
          next_s <= next_s;
      end if ;
    when bit_1_s =>
      if command_bit_1 = '0' then
        next_s <= reset_s;
      else
          next_s <= next_s;
      end if ;
    when last_bit_s =>
      if command_bit_1 = '0' then
        next_s <= tempo_s;
      else
          next_s <= last_bit_s;
      end if ;
    when tempo_s =>
      if command_tempo = '0' then
        next_s <= reset_s;
      else
          next_s <= next_s;
      end if ;
    when others =>
      next_s <= next_s;
  end case ;
end if ;
end process ; -- next_state

-- clocked               : Triggering signal : clk, reset
--                         Process synchronized to the input clock that change
--                         the current state with its next value.
clocked : process( clk, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;