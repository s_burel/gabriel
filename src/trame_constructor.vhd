------------------------------------------------------------------------------
-- frame_constructor
-- This component receive a 32 bits value from the frame_register
-- as a command from the user.
-- It decrypt the command and then output the decrypted command as
-- a full DCC packet
-- This component is a combinatorial circuit.
------------------------------------------------------------------------------
-- The incomming bits from the frame register are coded as :
-- Bits  0 to  7 : address
-- Bits  8 to 23 : Bits used to build the frame
--                 Contains all the functions sent and the way to code them
--                 Can change according to the group of functions (4 groups :
--                 function 0 to 4, 5 to 8, 9 to 12 and speed function)
-- Bit 24        : Validy bit
-- Bits 25 to 31 : Yet unused, but can be used if we then use more functions
------------------------------------------------------------------------------
-- There are plenty of different functions described by the NMRA standard,
-- but our project deals with only 21 differents functions able
-- to be run by your DCC models
-- Function 00 -> Light on/off
-- Function 01 -> All sounds on/off
-- Function 02 -> Sound : Horn #1
-- Function 03 -> Sound : Horn #2
-- Function 04 -> Sound : Turbo Engine
-- Function 05 -> Sound : Compressor
-- Function 06 -> Sound : Acceleration
-- Function 07 -> Sound : Squeak of curve
-- Function 08 -> Sound : Railway clank
-- Function 09 -> Sound : Fan
-- Function 10 -> Sound : Signal
-- Function 11 -> Sound : Short Horn #1
-- Function 12 -> Sound : Short Horn #2
-- Function 13 -> Sound : Railway Station Announcement #1
-- Function 14 -> Sound : Railway Station Announcement #2
-- Function 15 -> Sound : Alert signal #1
-- Function 16 -> Sound : Alert signal #2
-- Function 17 -> Sound : Doors open/closed
-- Function 18 -> Sound : Valve
-- Function 19 -> Sound : Hitch
-- Function 20 -> Sound : Sand
-- Function 21 -> Set speed of your model
------------------------------------------------------------------------------
-- These differents functions are grouped in 5 differents groups, according
-- to the coding of these function in the DCC packet.
-- Group 1 : Function 0 to 4 :
--     Coded on one byte in DCC packet as :
--         100XXXXX -> (for each X (1 = function on)(0 = function off))
--             Bit 4      : Function 0
--             Bit 3 to 0 : Function 4 to 1
-- Group 2 : Function 5 to 8 :
--     Coded on one byte in DCC packet as :
--         1011XXXX -> (for each X (1 = function on)(0 = function off))
--             Bit 3 to 0 : Function 8 to 5
-- Group 3 : Function 9 to 12 :
--     Coded on one byte in DCC packet as :
--         1010XXXX -> (for each X (1 = function on)(0 = function off))
--             Bit 3 to 0 : Function 12 to 9
-- Group 4 : Function 13 to 20 :
--     Coded on two bytes in DCC packet as :
--         11011110 XXXXXXXX
--             Bits 7 to 0 off second byte : Function 20 to 13
-- Group 5 : Speed function :
--     Coded on one byte on DCC packet as :
--         01XXXXXX -> (each X describe the commanded speed)
------------------------------------------------------------------------------
-- So a DCC packet can contains several differents bytes :
-- One byte as adress
-- One to Three bytes (our Project just deal with two of them) as command
-- One byte used as control of data
-- A DCC packet is built as :
-- Preamble : 14 bits to 1
-- Start Bit : 1 bit to 0
-- Address Byte : 8 bits
-- For each command byte : 
--   1 start bit (value : 0),
--   followed by the command byte
-- Start Bit : 1 Bit to 0
-- Control byte (XOR of each previous bytes)
-- Stop Bit : 1 Bit : value 1
------------------------------------------------------------------------------
-- Ports :
-- in : tr_to_tc_order : std_logic_vector(32) : Order received by the frame
--                         register and coded as :
--                         Bits  0 to  7 : address
--                         Bits  8 to 23 : Bits used to build the frame
--                                         Contains all the functions sent and
--                                         the way to code them. Can change
--                                         according to the group of functions
--                                         (4 groups : functions 0 to 4,
--                                         5 to 8, 9 to 12 and speed function)
--                         Bit 24        : Validy bit
--                         Bits 25 to 31 : Yet unused, but can be used if we
--                                         then use more functions
-- out : frame_value  : std_logic(60) : DCC packet translated.
--                                      Coded as NMRA standard DCC packet
--       frame_status : std_logic(3)  : Status on DCC packet translater
--                                      Indicate validity and length of the
--                                      out DCC frame
--                                        (0 -> invalid)
--                                         1 -> DCC contains 1 byte command
--                                         2 -> DCC contains 2 byte command
--                                         4 -> DCC contains 3 byte command
------------------------------------------------------------------------------
-- Used signals and variables
--   address        : Address of the DCC model you want to control
--   first_byte     : First command byte of the DCC packet
--   second_byte    : Second command byte of the DCC packet
--   third_byte     : Third command byte of the DCC packet
--   xor_byte       : The Control bit is a XOR of the previous bytes,
--                    it is used to detect transmission errors
--   frame_length   : Used to store the length of the DCC packet
--   frame_validity : Used to store the validity of the DCC packet
------------------------------------------------------------------------------
-- List of VHDL process :
-- address_handler      : Triggering signal : tr_to_tc_order(7 downto 0)
--                        That process handle the value of the address
--                        register
-- byte handler         : Triggering signal : tr_to_tc_order(23 downto 8)
--                        That process handle the three byte of command
-- frame_length_handler : Triggering signal : tr_to_tc_order(15 downto 8)
--                        That process handle the register that store the
--                        DCC packet length
-- xor_value            : Triggering signal : command bytes, address
--                        That process handle the register that store the
--                        command value
-- export               : Triggering signal : third and second byte, address
--                                            and frame_length
--                        Update out port DCC frame
-- validity_handler     : Triggering signal : frame_length, order_validity
--                        Update out port DCC validity
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity frame_constructor is
   port( tr_to_tc_order : in  std_logic_vector(31 downto 0);
         frame_value    : out std_logic_vector (59 downto 0);
         frame_status   : out std_logic_vector (2 downto 0));
end frame_constructor;
--
architecture behaviour of frame_constructor is

  signal address        : std_logic_vector(7 downto 0);
  signal first_byte     : std_logic_vector(7 downto 0);
  signal second_byte    : std_logic_vector(7 downto 0);
  signal third_byte     : std_logic_vector(7 downto 0);
  signal xor_value      : std_logic_vector(7 downto 0);
  signal frame_length   : std_logic_vector(2 downto 0);
  signal order_validity : std_logic;
  signal useless_signal_to_remove_warning :
                          std_logic_vector(6 downto 0);
begin

order_validity <= tr_to_tc_order(24);

useless_process_to_remove_warnings : 
process (tr_to_tc_order)
begin
    useless_signal_to_remove_warning <= tr_to_tc_order(31 downto 25);
end process;

-- address_handler      : Triggering signal : tr_to_tc_order(7 downto 0)
--                        That process handle the value of the address
--                        register
adress_handler : process( tr_to_tc_order(7 downto 0) )
begin
  address <= tr_to_tc_order(7 downto 0);
end process ; -- adress_handler

-- byte handler         : Triggering signal : tr_to_tc_order(23 downto 8)
--                        That process handle the three byte of command
byte_handler : process( tr_to_tc_order(23 downto 8) )
begin
  -- Speed control
  if tr_to_tc_order(15 downto 14) = "01" then
    first_byte  <= "01" & tr_to_tc_order(21 downto 16);
    second_byte <= "00000000";
    third_byte  <= "00000000";
  -- Function 0 to 4
  elsif tr_to_tc_order(15 downto 13) = "100" then
    first_byte  <= "100" & tr_to_tc_order(20 downto 16);
    second_byte <= "00000000";
    third_byte  <= "00000000";
  -- Function 5 to 8
  elsif tr_to_tc_order(15 downto 12) = "1011" then
    first_byte  <= "1011" & tr_to_tc_order(19 downto 16);
    second_byte <= "00000000";
    third_byte  <= "00000000";
  -- Function 9 to 12
  elsif tr_to_tc_order(15 downto 12) = "1010" then
    first_byte  <= "1010" & tr_to_tc_order(19 downto 16);
    second_byte <= "00000000";
    third_byte  <= "00000000";
  -- Function 13 to 20
--  elsif tr_to_tc_order(15 downto 8) = "11011110" then
  else
    first_byte  <= "11011110";
    second_byte <= tr_to_tc_order(23 downto 16);
    third_byte  <= "00000000";
  end if ;
end process ; -- byte_handler

-- frame_length_handler : Triggering signal : tr_to_tc_order(15 downto 8)
--                        That process handle the register that store the
--                        DCC packet length
frame_length_handler : process( tr_to_tc_order(15 downto 8) )
begin
  if tr_to_tc_order(15 downto 14) = "01" then
    frame_length <= "001";
  elsif tr_to_tc_order(15 downto 14) = "10" then
    frame_length <= "001";
  elsif tr_to_tc_order(15 downto 8) = "11011110" then
    frame_length <= "010";
  else
    frame_length <= "000";
  end if ;
end process ; -- frame_length_handler

-- xor_value            : Triggering signal : command bytes, address
--                        That process handle the register that store the
--                        command value
xor_handler : process( first_byte, second_byte, third_byte, address )
begin
  for i in 0 to 7 loop
    xor_value(i)
     <= first_byte(i) xor second_byte(i) xor third_byte(i) xor address(i);
  end loop ; --
end process ; --

-- export               : Triggering signal : third and second byte, address
--                                            and frame_length
--                        Update out port DCC frame
export : process( third_byte, second_byte, xor_value, frame_length )
begin
    case( frame_length ) is
      when "001" =>
      -- Bytes are transmitted from MSB to LSB, so :
        frame_value(33) <= xor_value(7);
        frame_value(34) <= xor_value(6);
        frame_value(35) <= xor_value(5);
        frame_value(36) <= xor_value(4);
        frame_value(37) <= xor_value(3);
        frame_value(38) <= xor_value(2);
        frame_value(39) <= xor_value(1);
        frame_value(40) <= xor_value(0);
        frame_value(41) <= '1';
        frame_value(59 downto 42)
            <= "000000000000000000";
      when "010" =>
      -- Bytes are transmitted from MSB to LSB, so :
        frame_value(33) <= second_byte(7);
        frame_value(34) <= second_byte(6);
        frame_value(35) <= second_byte(5);
        frame_value(36) <= second_byte(4);
        frame_value(37) <= second_byte(3);
        frame_value(38) <= second_byte(2);
        frame_value(39) <= second_byte(1);
        frame_value(40) <= second_byte(0);        
        frame_value(41) <= '0';
      -- Bytes are transmitted from MSB to LSB, so :
        frame_value(42) <= xor_value(7);
        frame_value(43) <= xor_value(6);
        frame_value(44) <= xor_value(5);
        frame_value(45) <= xor_value(4);
        frame_value(46) <= xor_value(3);
        frame_value(47) <= xor_value(2);
        frame_value(48) <= xor_value(1);
        frame_value(49) <= xor_value(0);
        frame_value(50) <= '1';
        frame_value(59 downto 51)
           <= "000000000";
      when others =>       
      -- Bytes are transmitted from MSB to LSB, so :
        frame_value(33) <= second_byte(7);
        frame_value(34) <= second_byte(6);
        frame_value(35) <= second_byte(5);
        frame_value(36) <= second_byte(4);
        frame_value(37) <= second_byte(3);
        frame_value(38) <= second_byte(2);
        frame_value(39) <= second_byte(1);
        frame_value(40) <= second_byte(0);        
        frame_value(41) <= '0';
      -- Bytes are transmitted from MSB to LSB, so :
        frame_value(42) <= third_byte(7);
        frame_value(43) <= third_byte(6);
        frame_value(44) <= third_byte(5);
        frame_value(45) <= third_byte(4);
        frame_value(46) <= third_byte(3);
        frame_value(47) <= third_byte(2);
        frame_value(48) <= third_byte(1);
        frame_value(49) <= third_byte(0);
        frame_value(50) <= '0';
      -- Bytes are transmitted from MSB to LSB, so :
        frame_value(51) <= xor_value(7);
        frame_value(52) <= xor_value(6);
        frame_value(53) <= xor_value(5);
        frame_value(54) <= xor_value(4);
        frame_value(55) <= xor_value(3);
        frame_value(56) <= xor_value(2);
        frame_value(57) <= xor_value(1);
        frame_value(58) <= xor_value(0);
        frame_value(59) <= '1';
--      when others =>
--        NULL;
    end case ;
end process ; -- 

-- validity_handler     : Triggering signal : frame_length, order_validity
--                        Update out port DCC validity
validity_handler : process( frame_length, order_validity )
begin
  if order_validity = '1' then
    frame_status <= frame_length;
  else
    frame_status <= "000";
  end if ;
end process ; -- validity_handler

------------------------------------------------------------------------------
-- Set frame for the bits that always have the same value
------------------------------------------------------------------------------
-- 02-15 : 14 bits : always set to 1
frame_value(13 downto 0) <= "11111111111111";
frame_value(14) <= '0';
-- Bytes are transmitted from MSB to LSB, so :
frame_value(15) <= address(7);
frame_value(16) <= address(6);
frame_value(17) <= address(5);
frame_value(18) <= address(4);
frame_value(19) <= address(3);
frame_value(20) <= address(2);
frame_value(21) <= address(1);
frame_value(22) <= address(0);
frame_value(23) <= '0';
-- Bytes are transmitted from MSB to LSB, so :
frame_value(24) <= first_byte(7);
frame_value(25) <= first_byte(6);
frame_value(26) <= first_byte(5);
frame_value(27) <= first_byte(4);
frame_value(28) <= first_byte(3);
frame_value(29) <= first_byte(2);
frame_value(30) <= first_byte(1);
frame_value(31) <= first_byte(0);
frame_value(32) <= '0';
------------------------------------------------------------------------------

end behaviour;