-------------------------------------------------------------
-- Finite State Machine
-------------------------------------------------------------
-- This component
-------------------------------------------------------------
-- List of different states :
-- Reset :
--   -> fsm = Z
--   -> next = Display
--   -> trame = IDLE
--   -> cpt = 0
-- Display :
--   -> command_fsm = 1
--   -> register_end = 0
--   -> register_value = tram[cpt]
--   -> next = inc
-- Inc :
--   -> cpt ++
--   -> next = wait
-- Wait :
--   -> fsm = Z
--   -> if fsm = 0 and
--         cpt /= max -> next = Display
--   -> if fsm = 0 and
--         cpt = max -> next = last
-- Last :
--   -> end = 1
--   -> fsm = 1
--   -> register_value = tram[cpt]
--   -> next = New_Trame
-- New_Trame :
--   -> cpt = 0
--   -> if fsm = 0 and
--         valid = 1 -> next = Display
--                      trame = trame_in
--   -> if fsm = 0 and
--         valid = 0 -> next = Display
--                      trame = idl
-------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dcc_register is
   port( clk            : in    std_logic;
         reset          : in    std_logic;
         trame_value    : in    std_logic_vector (59 downto 0);
         trame_status   : in    std_logic_vector (2 downto 0);
         register_end   : out   std_logic;
         register_value : out   std_logic;
         command_fsm    : inout std_logic);
end dcc_register;
--

architecture behaviour of dcc_register is

  type state_type is 
    ( reset_s, display_s, inc_s, wait_s , last_s, new_trame_s );

  signal current_s : state_type;
  signal next_s    : state_type;
  signal cpt : integer range 0 to 60;
  signal max : integer range 0 to 60;
  signal trame : std_logic_vector (59 downto 0);
  constant idle_trame : std_logic_vector(59 downto 0)
  := "111111111111110111111110000000000111111111000000000000000000";
begin

max_handler : process( current_s, trame_status )
begin
  case( current_s ) is
    when reset_s =>
      max <= 41;
    when new_trame_s =>
      if trame_status = "000" then
        max <= 41;
      elsif trame_status = "001" then
        max <= 41;
      elsif trame_status = "010" then
        max <= 50;
      elsif trame_status = "100" then
        max <= 59;
      end if ;
    when others =>
      NULL;
  end case ;  
end process ; -- max_handler

trame_handler : process( current_s, trame_status )
begin
  case( current_s ) is
    when reset_s =>
      trame <= idle_trame;
    when new_trame_s =>
      if trame_status = "000" then
        trame <= idle_trame;
      else
        trame <= trame_value;
      end if ;
    when others =>
      NULL;
  end case ;
end process ; -- trame_handler

cpt_handler : process( current_s )
begin
  case( current_s ) is
    when reset_s =>
      cpt <= 0;
    when new_trame_s =>
      cpt <= 0;
    when inc_s =>
      cpt <= cpt + 1;
    when others =>
      NULL;
  end case ;
end process ; -- cpt_handler

register_value_handler : process( current_s )
begin
  register_value <= trame(cpt);
end process ; -- register_value_handler

register_end_handler : process( cpt )
begin
  if cpt = max then
    register_end <= '1';
  else
    register_end <= '0';
  end if ;
end process ; -- register_end_handler

command_fsm_handler : process( current_s )
begin
  case( current_s ) is
    when display_s =>
      command_fsm <= '1';
    when last_s =>
      command_fsm <= '1';
    when others =>
      command_fsm <= 'Z';  
  end case ;
end process ; -- command_fsm_handler

next_state : process
  ( current_s, command_fsm )
begin
  case( current_s ) is
    when reset_s =>
      next_s <= display_s;
    when display_s =>
      next_s <= inc_s;
    when inc_s =>
      next_s <= wait_s;
    when wait_s =>
      if command_fsm = '0' and cpt < max then
        next_s <= display_s;
      elsif command_fsm = '0' and cpt = max then
        next_s <= last_s;
      end if ;
    when last_s =>
      next_s <= new_trame_s;
    when new_trame_s =>
      if command_fsm = '0' then
        next_s <= display_s;        
      end if ;
    when others =>
      NULL;
  end case ;
end process ; -- next_state

clocked : process( clk, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;