------------------------------------------------------------------------------
-- DCC Register
------------------------------------------------------------------------------
-- That component handle a register that store a full DCC packet.
-- Then the DCC is passed on bit after another to the FSM component.
-- When a full DCC is passed, the register is updated.
-- If the input DCC packet is valid, then the register is updated to its value
-- else, the register is updated to an IDLE packet, used to power the DCC
-- models without change its behaviour
------------------------------------------------------------------------------
-- Ports :
-- in    clk            : std_logic     : Clock used to control the fsm.
--       reset          : std_logic     : Used to reset the component
--       frame_value    : std_logic(60) : Full DCC packet passed by the
--                                        frame constructor component
--       frame_status   : std_logic(3)  : Validity of the DCC packet passed
--                                        by the frame constructor component
-- out   register_end   : std_logic     : Bit used to inform the FSM
--                                        component that the passed bit is
--                                        the last one of the DCC packet
--                                        Used to know if the FSM component
--                                        should then start the temporization
--                                        between two DCC packets
--       register_value : std_logic     : Bit of the DCC frame sent to the
--                                        FSM component.
-- inout command_fsm    : std_logic     : Flip-Flop used, as a mailbox to
--                                        synchronize the FSM component with
--                                        the DCC register component.
--                                        The value 1 -> Mailbox full
--                                        The value 0 -> Mailbox empty
------------------------------------------------------------------------------
-- Used signals and variables
-- current_s      : Define the state your component is actually in
-- next_s         : Define the state your component will enter in on the next
--                  rising edge of the clock
-- cpt            : Counter used as index of the DCC packet
--                  Define the next bit that will be sent to the FSM component
-- max            : Index of the last valid bit from the DCC packet
-- frame          : Register that store the DCC packet ot be sent to the 
--                  FSM component
------------------------------------------------------------------------------
-- Different state of the finite state machine :
-- reset_s       : (initial state) The counter is reset to 0, the max value
--                 is set to 41, the DCC packet register is set to idle value
--                 and the next state value is always the display state.
-- display_s     : Fill the mailbox flip-flop with the value 1.
--                 Next state is always inc_s
-- inc_s         : Increase the counter
--                 Next state is always wait_s
-- wait_s        : Wait for the acknowledge of the fsm component.
--                 If the flip-flop value is 1 -> next state still wait_s
--                 If the flip-flop value is 0 :
--                     If the counter value equals the max value, then next
--                         state is last state
--                     Else, then next state is display state
-- last_s        : Fill the mailbox flip-flop with the value 1.
--                 Next state is always new_frame_s
-- new_frame_s   : Update the counter, the DCC register and the max value
--                 Wait for the acknowledge of the fsm component
--                 If the flip-flop value is 1 -> next state still the same
--                 Else the next state is display_s
------------------------------------------------------------------------------
-- List of VHDL process :
-- validity_out_handler   : Triggering signal : current_s
--                          That process handle the value of the validity bit
--                          of the output order.
-- max_handler            : Triggering signal : current_s, frame_status
--                          Process that handle the max variable. Updated
--                          only if the current state is reset or new frame
--                          state
-- frame_handler          : Triggering signal : current_s, frame_status
--                          Process that handle the DCC packet register.
--                          Updated only if the current state is reset or
--                          new frame state
-- cpt_handler            : Triggering signal : current_s
--                          Process that handle the counter
-- register_value_handler : Triggering signal : current_s
--                          Process that handle the out value
-- register_end_handler   : Triggering signal : cpt
--                          Process that handle the register_end value, that
--                          inform the FSM component that the passed bit is
--                          the last of the DCC packet
-- command_fsm_handler    : Triggering signal : current_s
--                          Process that handle the flip-flop mailbox used
--                          to synchronize the FSM component with the
--                          DCC register component
-- next_state           : Triggering signal : current_s, command_fsm
--                        Process used to handle the next_state value
-- clocked              : Triggering signal : clk, reset
--                        Process synchronized to the input clock that change
--                        the current state with its next value.
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dcc_register is
   port( clk            : in    std_logic;
         reset          : in    std_logic;
         frame_value    : in    std_logic_vector (59 downto 0);
         frame_status   : in    std_logic_vector (2 downto 0);
         register_end   : out   std_logic;
         register_value : out   std_logic;
         command_fsm    : inout std_logic);
end dcc_register;
--

architecture behaviour of dcc_register is

  type state_type is 
    ( reset_s, display_s, inc_s, wait_s , last_s, new_frame_s );

  signal current_s : state_type := reset_s;
  signal next_s    : state_type;
  signal cpt : integer range 0 to 60;
  signal max : integer range 0 to 60;
  signal frame : std_logic_vector (59 downto 0)
  --constant idle_frame : std_logic_vector(59 downto 0)
  
  := "000000000000000000111111111000000000011111111011111111111111";
  --:= "000000000000000000110100110000100110010000000011111111111111";
  --  := "000000000000000000111001001000001001011000000011111111111111";
    -- Move 1 forward step 5
    -- adress                 00000001 10000000
    -- command                01100100 00100110
    -- xor                    01100101 10100110
    -- Light on 3
    -- address                00000011 11000000
    -- command                
  
  
    --:= "0000000000000000001 10100110 0 00100110 0 10000000 011111111111111";
  
  attribute keep : string;
  
    attribute keep of max : signal is "true";
    attribute keep of frame : signal is "true";
  
begin

max_handler : process( clk )
begin
if rising_edge(clk) then
  case( current_s ) is
    when reset_s =>
      max <= 41;
    when new_frame_s =>
      if frame_status = "001" then
        max <= 41;
      elsif frame_status = "010" then
        max <= 50;
      elsif frame_status = "100" then
        max <= 59;
      else
        max <= 41;
      end if ;
    when others =>
      max <= max;
  end case ;
end if ;
end process ; -- max_handler

frame_handler : process( clk )
begin
if rising_edge(clk) then
  case( current_s ) is
    when reset_s =>
      frame <= frame;
    when new_frame_s =>
      if frame_status = "001" then
        frame <= frame_value;
      elsif frame_status = "010" then
        frame <= frame_value;
      elsif frame_status = "100" then
        frame <= frame_value;
      else
        frame <= frame;
      end if ;
    when others =>
        frame <= frame;
  end case ;
end if ;
end process ; -- frame_handler

cpt_handler : process( clk )
begin
if rising_edge(clk) then
  case( current_s ) is
    when reset_s =>
      cpt <= 0;
    when new_frame_s =>
      cpt <= 0;
    when inc_s =>
      if next_s = wait_s then
        cpt <= cpt + 1;
      else
        cpt <= cpt;
      end if;
    when others =>
      cpt <= cpt;
  end case ;
end if;
end process ; -- cpt_handler

register_value_handler : process( current_s, frame, cpt )
begin
  register_value <= frame(cpt);
end process ; -- register_value_handler

register_end_handler : process( cpt, current_s, max )
begin
  if current_s = reset_s then
    register_end <= '0';    
  elsif cpt = max then
    register_end <= '1';
  else
    register_end <= '0';
  end if ;
end process ; -- register_end_handler

command_fsm_handler : process( current_s )
begin
  case( current_s ) is
    when display_s =>
      command_fsm <= '1';
    when last_s =>
      command_fsm <= '1';
    when others =>
      command_fsm <= 'Z';  
  end case ;
end process ; -- command_fsm_handler

next_state : process(clk)
--  ( current_s, command_fsm )
begin
if rising_edge(clk) then
  case( current_s ) is
    when reset_s =>
      next_s <= display_s;
    when display_s =>
      next_s <= inc_s;
    when inc_s =>
      next_s <= wait_s;
    when wait_s =>
      if command_fsm = '0' and cpt /= max then
        next_s <= display_s;
      elsif command_fsm = '0' and cpt = max then
        next_s <= last_s;
      end if ;
    when last_s =>
      next_s <= new_frame_s;
    when new_frame_s =>
      if command_fsm = '0' then
        next_s <= display_s;
      else
        next_s <= next_s;
      end if ;
    when others =>
      next_s <= next_s;
  end case ;
end if ;
end process ; -- next_state

clocked : process( clk, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;