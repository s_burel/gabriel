------------------------------------------------------------------------------
-- DCC Bit 1 Source
------------------------------------------------------------------------------
-- That component is commanded by the FSM and when its ordered, it generate
-- A DCC bit value of 1, which mean its out value is 1 for 58us, and then 0
-- for the next 58us.
-- It works according to a simple Finite State Machine and its cadenced by
-- two clocks :
--   100 Mhz Clock used to manage the FSM
--   1 Mhz used to manage the counter
------------------------------------------------------------------------------
-- Ports :
--   in    clk_fsm     : std_logic : Clock used to control the fsm
--         clk_tempo   : std_logic : Clock used to control the counter
--         reset       : std_logic : Used to reset the component
--   inout command_1   : std_logic : Flip-flop to synchronize the DCC Bit 1
--                                   component and the FSM component
--   out   bit_1_value : std_logic : Out value of this component
------------------------------------------------------------------------------
-- Used signals and variables
--   current_s : Define the state your component is actually in
--   next_s    : Define the state your component will enter in on the next
--               rising edge of the clock
--   cpt       : Counter used to count the numer of rising edge
--               of the counter clock
------------------------------------------------------------------------------
-- Different state of the finite state machine :
--   idle_s     : (initial state)
--                If flip-flop equal 1, then next state is start_s,
--                Else next state still idle_s
--                Flip-flop set to High Impedance, wait for
--                  the flip-flop to get the 1 value, which mean a command 
--                  from the FSM component for the generation of a 1 bit.
--                Out Value is set to 0.
--                Used at reset state.
--   start_s    : Set the counter to 0
--                Out Value is set to 1
--                Flip-flop value set to 1
--                Next state is always inc0_s
--   inc0_s     : Increment the counter until its value equal 58,
--                  then next state is set to overflow_s
--                Flip-flop value set to 1
--   overflow_s : Set the Out value to 1
--                Flip-flop value set to 1
--                Set the counter to 0
--                Next state is always inc1_s
--   inc1_s     : Increment the counter until its value equal 58,
--                  then next state is set to end_s
--                Flip-flop value set to 1
--   end_s      : Flip-flop value set to 0 the inform the FSM component that
--                  the bit 1 generation ends
--                Next state is always idle_s
------------------------------------------------------------------------------
-- List of VHDL process :
-- cpt_handler         : Signal triggerer : clk_tempo, current_s
--                         That process handle the counter, increasing the
--                         counter on each rising edge of the input clock,
--                         except when the current state of the FSM is the
--                         start state or the overflow state, in which case
--                         the counter is reset to 0
-- command_1_handler   : Signal triggerer : current_s
--                         That process handle the command_1 flip-flop
-- bit_1_value_handler : Signal triggerer : current_s
--                         That process handle the bit_1_value port
-- next_state          : Signal triggerer : current_s, cpt, command_1
--                         Process used to handle the next_state and the out
--                         register according to the current state
-- clocked             : Signal triggerer : clk_fsm, reset
--                         Process synchronized to the input clock that change
--                         the current state with its next value.
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dcc_bit_1 is
   port( clk_fsm     : in  std_logic;
         clk_tempo   : in  std_logic;
         reset       : in  std_logic;
         command_1   : inout std_logic;
         bit_1_value : out std_logic);
end dcc_bit_1;
--
architecture behaviour of dcc_bit_1 is

  type state_type is (idle_s, start_s, inc0_s, inc1_s, overflow_s, end_s);

  signal current_s : state_type := idle_s;
  signal next_s    : state_type;

  signal cpt : integer range 0 to 120;

begin  

-- cpt_handler         : Signal triggerer : clk_tempo, current_s
--                         That process handle the counter, increasing the
--                         counter on each rising edge of the input clock,
--                         except when the current state of the FSM is the
--                         start state or the overflow state, in which case
--                         the counter is reset to 0
cpt_handler : process( clk_tempo, current_s )
begin
  if current_s = start_s then
    cpt <= 0;
  elsif current_s = overflow_s then
    cpt <= 0;
  elsif rising_edge(clk_tempo) then
    if current_s = inc0_s then
      cpt <= cpt + 1;
    elsif current_s = inc1_s then
      cpt <= cpt + 1;
  end if ;
end if ;
end process ; -- cpt_handler

-- command_1_handler   : Signal triggerer : current_s
--                         That process handle the command_1 flip-flop
command_1_handler : process( next_s )
begin
  case( next_s ) is
    when end_s =>
      command_1 <= '0';
    when idle_s =>
      command_1 <= 'Z';
    when others =>
      command_1 <= '1';
  end case ;
end process ; -- command_1_handler

-- bit_1_value_handler : Signal triggerer : current_s
--                         That process handle the bit_1_value port
bit_1_value_handler : process( next_s )
begin
  case( next_s ) is
--  idle_s, start_s, inc0_s, inc1_s, overflow_s, end_s
      when start_s =>
        bit_1_value <= '0';
      when inc0_s =>
        bit_1_value <= '0';
      when idle_s =>
        bit_1_value <= '0';
      when others =>
        bit_1_value <= '1';
    end case ;  
end process ; -- bit_1_value_handler

-- next_state          : Signal triggerer : current_s, cpt, command_1
--                         Process used to handle the next_state and the out
--                         register according to the current state
next_state : process( clk_fsm )
begin
if rising_edge(clk_fsm) then
  case( current_s ) is
    when idle_s =>
      if command_1 = '1' then
        next_s <= start_s;
      else
        next_s <= idle_s;
      end if ;
    when start_s =>
      next_s <= inc0_s;
    when inc0_s =>
      if cpt = 58 then
        next_s <= overflow_s;
      else
        next_s <= next_s;
      end if ;
    when overflow_s =>
      next_s <= inc1_s;
    when inc1_s =>
      if cpt = 58 then
        next_s <= end_s;
      else
        next_s <= next_s;
      end if ;
    when end_s =>
      next_s <= idle_s;
    when others =>
      next_s <= next_s;
  end case ;
end if;
end process ; -- next_state

-- clocked             : Signal triggerer : clk_fsm, reset
--                         Process synchronized to the input clock that change
--                         the current state with its next value.
clocked : process( clk_fsm,     reset )
begin
  if reset = '0' then
    current_s <= idle_s;
  elsif rising_edge(clk_fsm) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;