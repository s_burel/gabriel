-------------------------------------------------------------
-- Top Source
-------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
   port( clk100mhz     : in  std_logic;
         reset   : in  std_logic;
         button_left   : in  std_logic;
         button_right  : in  std_logic;
         button_center : in  std_logic;
         leds          : out std_logic_vector(7 downto 0);
         value : out std_logic);
end top;
--
architecture behaviour of top is

   component clock_divisor
   port( clk     : in  std_logic;
         reset   : in  std_logic;
         clk_out : out std_logic);
   end component;

   component dcc_bit_1
   port( clk_fsm     : in  std_logic;
         clk_tempo   : in  std_logic;
         reset       : in  std_logic;
         command_1   : inout std_logic;
         bit_1_value : out std_logic);
   end component;

   component dcc_bit_0
   port( clk_fsm     : in  std_logic;
         clk_tempo   : in  std_logic;
         reset       : in  std_logic;
         command_0   : inout std_logic;
         bit_0_value : out std_logic);
   end component;

   component fsm
      port( clk            : in  std_logic;
         reset          : in  std_logic;
         register_end   : in  std_logic;
         register_value : in  std_logic;
         command_bit_0  : inout std_logic;
         command_bit_1  : inout std_logic;
         command_tempo  : inout std_logic;
         command_fsm    : inout std_logic);
   end component;

   component tempo
       port( clk_fsm     : in  std_logic;
         clk_tempo   : in  std_logic;
         reset      : in  std_logic;
         command    : inout std_logic);
   end component;

  component dcc_register is
    port( clk            : in    std_logic;
          reset          : in    std_logic;
          frame_value    : in    std_logic_vector (59 downto 0);
          frame_status   : in    std_logic_vector (2 downto 0);
          register_end   : out   std_logic;
          register_value : out   std_logic;
          command_fsm    : inout std_logic);
  end component;

  component frame_constructor is
    port( tr_to_tc_order : in  std_logic_vector(31 downto 0);
      frame_value    : out std_logic_vector (59 downto 0);
      frame_status   : out std_logic_vector (2 downto 0));
  end component;

  component frame_register is
    port( clk            : in std_logic;
          reset          : in std_logic;
          command        : in std_logic_vector(19 downto 0);
          tr_to_tc_order : out  std_logic_vector(31 downto 0));
  end component;

  component user_interface is
    port( clk           : in std_logic;
          reset         : in  std_logic;
          button_left   : in  std_logic;
          button_right  : in  std_logic;
          button_center : in  std_logic;
          command       : out std_logic_vector(19 downto 0);
          leds          : out std_logic_vector(7 downto 0));
  end component;


for top_clock_divisor:    clock_divisor     use entity work.clock_divisor;
for top_clock_10khz:    clock_divisor     use entity work.clock_divisor;
for top_dcc_bit_1:        dcc_bit_1         use entity work.dcc_bit_1;
for top_dcc_bit_0:        dcc_bit_0         use entity work.dcc_bit_0;
for top_fsm:              fsm               use entity work.fsm;
for top_tempo:            tempo             use entity work.tempo;
for top_dcc_register:     dcc_register      use entity work.dcc_register;
for top_frame_constructor:frame_constructor use entity work.frame_constructor;
for top_frame_register:   frame_register    use entity work.frame_register;

  -- Global signals
  signal clk1mhz : std_logic;
  signal clk10khz : std_logic;
  -- FSM <-> DCC signals
  signal command_0   : std_logic;
  signal command_1   : std_logic;
  -- FSM <-> Tempo
  signal command_tempo : std_logic;
  -- FSM <-> DCC Register
  signal register_end : std_logic;
  signal register_value : std_logic;
  signal command_fsm : std_logic;
  -- DCC signals <-> Xor OUT
  signal dcc_bit0_to_or : std_logic;
  signal dcc_bit1_to_or : std_logic;
  -- DCC Register <-> frame_constructor
  signal frame_value  : std_logic_vector (59 downto 0);
  signal frame_status : std_logic_vector (2 downto 0);
  -- frame_constructor <-> frame register
  signal tr_to_tc_order : std_logic_vector(31 downto 0);
  -- frame_register <-> user interface
  signal command        : std_logic_vector(19 downto 0);


begin  


  top_clock_divisor: clock_divisor port map (clk100mhz, reset, clk1mhz);
  top_clock_10khz: clock_divisor port map (clk1mhz, reset, clk10khz );
  top_dcc_bit_1: dcc_bit_1 port map (clk100mhz, clk1mhz, reset, command_1, dcc_bit1_to_or );
  top_dcc_bit_0: dcc_bit_0 port map (clk100mhz, clk1mhz, reset, command_0, dcc_bit0_to_or );
  top_tempo: tempo port map (clk100mhz, clk10khz, reset, command_tempo);
  top_fsm: fsm port map( clk100mhz, reset, register_end, register_value, command_0, command_1, command_tempo, command_fsm );
  top_dcc_register: dcc_register port map( clk100mhz, reset, frame_value, frame_status, register_end, register_value, command_fsm);
  top_frame_constructor: frame_constructor port map( tr_to_tc_order, frame_value, frame_status );
  top_frame_register: frame_register port map( clk100mhz, reset, command, tr_to_tc_order );
  top_user_interface: user_interface port map( clk10khz, reset, button_left, button_right, button_center, command, leds );
  --top_user_interface: user_interface port map( clk100mhz, reset, button_left, button_right, button_center, command, leds );

or_value : process( dcc_bit0_to_or, dcc_bit1_to_or )
begin
  value <= dcc_bit0_to_or or dcc_bit1_to_or;
end process ; -- or_value

end behaviour;