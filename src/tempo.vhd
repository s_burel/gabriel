------------------------------------------------------------------------------
-- Temporizer Source
------------------------------------------------------------------------------
-- That component is commanded by the FSM and when its ordered, it quantify
-- 60 rising edge of incoming clock (60 tick of 10kHZ -> 6 ms), before to
-- send a signal to the FSM component to warn the end of the temporizer
-- It works according to a simple Finite State Machine and its cadenced by
-- two clocks :
--   100 Mhz Clock used to manage the FSM
--   10 Khz used to manage the counter
------------------------------------------------------------------------------
-- Ports :
--   in    clk_fsm     : std_logic : Clock used to control the fsm
--         clk_tempo   : std_logic : Clock used to control the counter
--         reset       : std_logic : Used to reset the component
--   inout command     : std_logic : Flip-flop to synchronize the temporizer
--                                   and the FSM component
------------------------------------------------------------------------------
-- Used signals and variables
--   current_s : Define the state your component is actually in
--   next_s    : Define the state your component will enter in on the next
--               rising edge of the clock
--   cpt       : Counter used to count the numer of rising edge
--               of the counter clock
------------------------------------------------------------------------------
-- Different state of the finite state machine :
--   start_s    : (initial state)
--                Flip-flop set to High Impedance, wait for
--                  the flip-flop to get the 1 value, which mean a command 
--                  from the FSM component for the start of the temporizer.
--                Counter set to 0
--                If flip-flop equal 1, then next state is inc_s,
--                Else next state still start_s
--   reset_s    : Flip-flop set ot High Impedance.
--                Next state is always start staet.
--                Used at reset state.
--   inc_s      : Flip-flop value set to 1 until the counter equals 59,
--                in which case the next state is overflow_s, else the
--                state still inc_s.
--                Increment the counter value each rising edge of the
--                counter clock
--   overflow_s : Set the flip-flop value to 0 to signal to the FSM component
--                the end of the temporization
--                Next state is always start_s
------------------------------------------------------------------------------
-- List of VHDL process :
-- cpt_handler         : Triggering signal : clk_tempo
--                         That process handle the counter, increasing the
--                         counter on each rising edge of the counter clock
--                         if the current state is inc_s.
--                         If the current state is start_s, then the counter
--                         is set to 0
-- command_handler     : Triggering signal : current_s
--                         That process handle the flip-flop to synchronize
--                           the temporizer with the FSM component.
--                         The command is set to high impedence in the state
--                           where we are waiting for a signal from the FSM
--                           component (reset_s and start_s).
--                         The command is set to 0 in the overflow state to
--                           signal to the FSM component the end of the
--                           temporization
--                         The command is set to 1 in the other state to
--                           signal that the temporizeer is busy
-- next_state          : Triggering signal : current_s, cpt, command_0
--                         Process used to handle the next_state and the out
--                         register according to the current state
-- clocked             : Triggering signal : clk_fsm, reset
--                         Process synchronized to the input clock that change
--                         the current state with its next value.
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tempo is
   port( clk_fsm     : in  std_logic;
         clk_tempo   : in  std_logic;
         reset      : in  std_logic;
         command    : inout std_logic);
end tempo;
--

architecture behaviour of tempo is

  type state_type is (start_s, reset_s, inc_s, overflow_s);

  signal current_s : state_type := start_s;
  signal next_s    : state_type;

  signal cpt     : integer range 0 to 60; 

begin

-- cpt_handler         : Triggering signal : clk_tempo
--                         That process handle the counter, increasing the
--                         counter on each rising edge of the counter clock
--                         if the current state is inc_s.
--                         If the current state is start_s, then the counter
--                         is set to 0
cpt_handler : process( clk_tempo )
begin
if rising_edge(clk_tempo) then
  if current_s = start_s then
    cpt <= 0;
  elsif current_s = inc_s then
    cpt <= cpt + 1;
  end if ;
end if ;
end process ; -- cpt_handler

-- command_handler     : Triggering signal : current_s
--                         That process handle the flip-flop to synchronize
--                           the temporizer with the FSM component.
--                         The command is set to high impedence in the state
--                           where we are waiting for a signal from the FSM
--                           component (reset_s and start_s).
--                         The command is set to 0 in the overflow state to
--                           signal to the FSM component the end of the
--                           temporization
--                         The command is set to 1 in the other state to
--                           signal that the temporizeer is busy
command_handler : process( next_s )
begin
  case( next_s ) is
    when overflow_s =>
      command <= '0';
    when reset_s =>
      command <= 'Z';
    when start_s =>
      command <= 'Z';
    when others =>
      command <= '1';
  end case ;
end process ; -- command_handler


-- next_state          : Triggering signal : current_s, cpt, command_0
--                         Process used to handle the next_state and the out
--                         register according to the current state
next_state : process( clk_fsm )
begin
if (rising_edge(clk_fsm)) then
  case( current_s ) is
    when reset_s =>
      next_s <= start_s;
    when start_s =>
      if command = '1' then
        next_s <= inc_s;
      else
        next_s <= next_s ;
      end if ;
    when inc_s =>
      if cpt = 59 then
        next_s <= overflow_s;
      else
        next_s <= next_s ;
      end if ;
    when overflow_s =>
      next_s <= start_s;
    when others =>
      next_s <= next_s ;
  end case ;
end if;
end process ; -- next_state

-- clocked             : Triggering signal : clk_fsm, reset
--                         Process synchronized to the input clock that change
--                         the current state with its next value.
clocked : process( clk_fsm, reset )
begin
  if reset = '0' then
    current_s <= reset_s;
  elsif rising_edge(clk_fsm) then
    current_s <= next_s;
  end if ;
end process ; -- clocked

end behaviour;