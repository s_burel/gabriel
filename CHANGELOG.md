# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2018-05-01
### Added
- Changelog
- Readme
- XML Diagram and PNG conversion to Clock Divisor
- XML Diagram and PNG conversion to FSM
- XML Diagram and PNG conversion to DCC Register
- XML Diagram and PNG conversion to Top
- XML Diagram and PNG conversion to DCC Bit Generator
- XML Diagram and PNG conversion to Frame Register
- XML Diagram and PNG conversion to Temporizer
- XML Diagram and PNG conversion to User Interface
- Libre Office Text Describing the project and its pdf
- DCC Bit 0 Source
- DCC Bit 1 Source
- Frame Constructor Source
- Frame register Source
- FSM Source
- Tempo Source
- Top Source
- User Interface Source